<?php namespace App\Controllers;

class Page extends BaseController
{
	public function login()
	{
		return view("login");
	}
	
	
	public function fitur()
	{
		$data = [
			'title' => 'Fitur Lengkap | REQAPP+',
			'banner_title' => 'Fitur Lengkap'
		];
		return view("fitur", $data);
	}

	public function kontak()
	{
		$data = [
			'title' => 'Kontak Kami | REQAPP+',
			'banner_title' => 'Kontak Kami'
		];
		return view("kontak", $data);
	}

}