<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$data = [
			'title' => 'REQAPP+ | Aplikasi Manajemen Permintaan Kebutuhan Logistik',
		];
		return view('home', $data);
	}

	//--------------------------------------------------------------------

}
