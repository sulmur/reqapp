<section id="banner">
    <div class="container">
        <div class="d-flex align-items-center text-center banner-height">
            <div class="m-auto">
                <h1 class="marker-title"><?= $banner_title; ?></h1>
                <br>  
                <br> 
                <h5 class="marker"><a href="/">Beranda</a> / <?= $banner_title; ?></h5>
            </div>
        </div>
    </div>
    <div class="waver">
        <img src="images/background/wave-banner.svg" draggable="false" alt="">
    </div>
</section>