<?= $this->extend('layout/template'); ?>

<footer>
    <div class="container footer-pt">
        <div class="pb-5 mt-5">
            <div class="row  justify">
                <div class="col-lg-6 mobile-spacing">
                    <h4 class="marker-title mb-3">
                        REQ<span>APP+</span>
                    </h4>
                    <p class="text-justify title" style="">Gunakan REQAPP+ untuk mempermudah permintaan logistik dengan cepat dan mudah.</p>
                    <ul class="social-media">
                        <li><a href="#"><img src="images/medsos/linkedin.svg" alt=""></a></li>
                        <li><a href="#"><img src="images/medsos/twitter.svg" alt=""></a></li>
                        <li><a href="#"><img src="images/medsos/instagram.svg" alt=""></a></li>
                        <li><a href="#"><img src="images/medsos/facebook.svg" alt=""></a></li>
                    </ul>
                </div>
                <div class="col-lg-3 text-justify mobile-spacing">
                    <div class="footer-link d-flex justify-content-md-between pl-5">              
                        <div class="link-wrapper">
                            <div class="footer-title mb-3">
                                <h4 class="title">Sitemaps</h4>
                            </div>
                            <ul class="link list-unstyled mobile-spacing title-spacing">
                                <li><a data-scroll href="#home">Beranda</a></li>
                                <li><a data-scroll href="/fitur">Fitur Lengkap</a></li>
                                <li><a data-scroll href="/kontak">Kontak Kami</a></li>
                            </ul>
                        </div> 
                    </div> 
                </div>
                <div class="col-lg-3 text-justify mobile-spacing title-spacing">
                    <div class="footer-contact">
                        <div class="footer-title mb-3">
                            <h4 class="title">Kontak Kami</h4>
                        </div>
                        <ul class="contact list-unstyled">
                            <li><a data-scroll href="#home">(+(021) 2761-7679 (Kantor)</a></li>
                            <li><a data-scroll href="#">contact@abbauf.com</a></li>
                        </ul>
                        <a class="download" href="#">Download Our App</a>
                    </div> 
                </div>
            </div> 
        </div> 
    </div>
    <!-- <div class="text-center container-fluid pt-4 pb-4">
        <h6>© <?= date('Y') ?> PT Abbauf Mulia Konsultan Teknologi.</h6>
    </div> -->
</footer>