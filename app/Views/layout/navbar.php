<header id="home">
    <nav class="navbar navbar-expand-lg navbar-light bg-info fixed-top navbar-scroll p-3">
        <div class="container">
            <a class="navbar-brand" href="#">REQ<span>APP+</span></a>
        
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ml-3" id="berandaActive">
                        <a class="nav-link" href="/">Beranda</a>
                    </li>
                    <li class="nav-item ml-3" id="fiturActive">
                        <a class="nav-link" href="/fitur">Fitur Lengkap</a>
                    </li>
                    <li class="nav-item ml-3" id="kontak">                        
                        <a class="btn btn-success" href="/kontak">Kontak Kami</a>
                    </li>
                </ul>        
            </div>
        </div>
    </nav>
</header>