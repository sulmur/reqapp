<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Page Not Found</title>
	<link rel="icon" type="image/svg" href="images/logo.svg" />
	<style>
	div.logo {
		height: 200px;
		width: 155px;
		display: inline-block;
		opacity: 0.08;
		position: absolute;
		top: 2rem;
		left: 50%;
		margin-left: -73px;
	}
	body {
		height: 100%;
		background: #fafafa;
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		color: #777;
		font-weight: 300;
	}
	h1 {
		font-weight: lighter;
		letter-spacing: 0.8;
		font-size: 3rem;
		margin-top: 0;
		margin-bottom: 0;
		color: #222;
	}
	.wrap {
		max-width: 1024px;
		margin: 5rem auto;
		padding: 2rem;
		background: #fff;
		text-align: center;
		border: 1px solid #efefef;
		border-radius: 0.5rem;
		position: relative;
	}
	pre {
		white-space: normal;
		margin-top: 1.5rem;
	}
	code {
		background: #fafafa;
		border: 1px solid #efefef;
		padding: 0.5rem 1rem;
		border-radius: 5px;
		display: block;
	}
	p {
		margin-top: 1.5rem;
	}
	.footer {
		margin-top: 2rem;
		border-top: 1px solid #efefef;
		padding: 1em 2em 0 2em;
		font-size: 85%;
		color: #999;
	}
	a:active,
	a:link,
	a:visited {
		color: #dd4814;
	}

	.login-btn {
		width: 100px;
		height: 40px;
		-webkit-box-shadow: 0px 4px 6px 0px rgba(30,32,33,0.07);
		-moz-box-shadow: 0px 4px 6px 0px rgba(30,32,33,0.07);
		box-shadow: 0px 4px 6px 0px rgba(30,32,33,0.07);
		border-radius: 5px;
		font-size: 1.2rem;
		font-family: "Montserrat";
		font-weight: 400;
		background-color: #F79318;
		border:1px solid #F79318;
		color:#FEFEFE;
	}

	.login-btn:hover {
		background-color: #3B475F;
		border:1px solid #3B475F;
		color:#FEFEFE;
	}
</style>
</head>
<body>
	<div class="wrap">
		<h1>404 - File Not Found</h1>

		<p>
			<?php if (! empty($message) && $message !== '(null)') : ?>
				<?= esc($message) ?>
			<?php else : ?>
				Maaf, Halaman yang Anda cari tidak ditemukan!
			<?php endif ?>
		</p>
		<div class="text-center">
			<button onClick="redirect('home')" type="button" class="login-btn">Kembali</button>
		</div>
	</div>
	<script>
    function redirect(url) {
        location.href = url;
    }
  	</script>
</body>
</html>
