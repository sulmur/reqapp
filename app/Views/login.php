<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<div class="login-section">
    <div class="login">
        <div class="card-body">
            <div class="text-center">
                <h4 class="login-title">Sign in<h4>
            </div>
            <form method="post" action="/login/auth">
                <div class="form-group text-center" style="padding-left:2rem ; padding-right:2rem ;">
                    <input class="form-control" type="text" placeholder="Username" autoComplete="Username" name="username" required="required" /> <br>
                    <input class="form-control" type="password" placeholder="Password" autoComplete="Password" name="password" />
                </div>
                <div class="text-center">
                    <button onClick="redirect('data_peserta')" type="button" class="login-btn">Login</button>
                </div>
            </form>            
        </div>
    </div>
    <a class="mt-3" href="">Lupa Password?</a>
</div>

<?= $this->endSection(); ?>