<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<?= $this->include('layout/banner'); ?>

<section id="contact">
    <div class="container">
        <div class="text-center mt-5">
            <h2 class="marker-title mb-4">Kontak Kami</h2>
            <p class="marker">Info lebih lanjut, dapat mengubungi kontak kami berikut ini :</p>
        </div>
        <div class="row mt mb-5">
            <div class="col-md-6" data-aos="fade-right" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                <img src="images/icon/contact/contact-ilustration.svg" class="w-100 my-3" alt="">
            </div>
            <div class="col-md-6" data-aos="fade-left" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                <ul class="list-unstyled">
                    <li><h5 class="marker-title">ALAMAT</h5></li>
                    <li class="mb-4"><p class="marker">Pesona Khayangan Mungil 1 Blok G No. G8 RT 01 RW 029, Kel. Mekarjaya, Kec. Sukmajaya, Depok 16411</p></li>
                    <li><h5 class="marker-title">TELEPON</h5></li>
                    <li><p><img src="images/icon/contact/phone.svg" class="mr-3" alt="">+62-853-5724-5393 (Alfyan)</p></li>
                    <li class="mb-4"><p><img src="images/icon/contact/phone.svg" class="mr-3" alt="">(021) 2761-7679 (Kantor)</p></li>
                    <li><h5 class="marker-title">LAYANAN ONLINE</h5></li>
                    <li><p><img src="images/icon/contact/web.svg" class="mr-3" alt="">www.abbauf.com</p></li>
                    <li><p><img src="images/icon/contact/email.svg" class="mr-3" alt="">contact@abbauf.com</p></li>
                </ul>
            </div>
        </div>
        <div class="mb-5">
            <iframe data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="800" data-aos-offset="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.096271942526!2d106.84146701476995!3d-6.381573895382335!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69eb8f14c42065%3A0x5027cd751b952f13!2sPT%20Abbauf%20Mulia%20Konsultan%20Teknologi!5e0!3m2!1sen!2sid!4v1614250186665!5m2!1sen!2sid" class="map" allowfullscreen="" loading="lazy"></iframe>
        </div>
    </div>
    <img class="contact-footer" src="images/background/wave-footer.svg" draggable="false" alt=""> 
</section>

<?= $this->include('layout/footer'); ?>
<?= $this->endSection(); ?>