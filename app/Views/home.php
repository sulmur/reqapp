<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<section id="top-home">
    <div class="container">
        <!-- <div class="justify">
            <div class="hero">
                <img src="images/background/ilustrasi.svg" draggable="false" alt="">
            </div>
            <div class="title">
                <div data-aos="fade-down" data-aos-duration="1500">
                    <h1 class="marker-title" >Lebih Mudah dan Cepat <br>Dengan REQ<span>APP+</span></h1>
                    <p class="text-justify marker" >
                        REQAPP+ hadir sebagai solusi dalam pendataan logistik instansimu. Membantu manajemen data logistik yang dapat diakses dimana pun dan kapan pun.
                    </p>
                </div>
                    <a class="btn btn-success mt-5" data-aos="fade-up" data-aos-duration="1500" data-scroll href="#">
                        COBA SEKARANG
                    </a>
            </div>
        </div> -->
        <div class="row mt mb-5">
            <div class="col-md-6 col-xs-12">
                <div class="title">
                    <div data-aos="fade-down" data-aos-duration="1500">
                        <h1 class="marker-title" >Lebih Mudah dan Cepat <br>Dengan REQ<span>APP+</span></h1>
                        <p class="text-justify marker mt-5" >
                            REQAPP+ hadir sebagai solusi dalam pendataan logistik instansimu. Membantu manajemen data logistik yang dapat diakses dimana pun dan kapan pun.
                        </p>
                    </div>
                        <a class="btn btn-success mt-5" data-aos="fade-up" data-aos-duration="1500" data-scroll href="#">
                            COBA SEKARANG
                        </a>
                </div>
            </div>
            <div class="col-md-6 col-xs-12" data-aos="fade-left" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                <img src="images/background/ilustrasi.svg" draggable="false" class="w-90" style="margin: 8rem 5rem;" alt="">
            </div>
        </div>
    </div>
</section> 
<section id="welcome">
    <div class="container">
        <img class="wave" src="images/background/home-top-banner.svg" draggable="false" alt="">
        <img class="phone" src="images/background/phone-ilustration.svg" alt="">
        <div class="row justify">
            <div class="col-7"></div>
            <div class="col-5">
                <div data-aos="fade-left" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                    <h2 class="marker-title">Welcome to REQ<span>APP+</span></h2>
                    <div class="line-divider"></div>
                    <p class="marker text-justify" style="max-width:100%; line-height:2rem">
                        REQAPP (Request App) adalah sebuah aplikasi yang memungkinkan pengguna melakukan permintaan langsung kebutuhannya lewat aplikasi, pengguna juga dapat memantau progress kebutuhan yang diminta, serta pengguna juga dapat melihat laporan dan visualisasi data permintaan dengan baik melalui tampilan tabel dan grafik.
                    </p>
                </div>
                <a class="btn btn-success mt-4" data-aos="fade-zoom-in" data-aos-duration="1200" data-scroll href="#">
                  LIVE DEMO
                </a>
            </div>
        </div>
    </div>
</section>   
<section id="keunggulan">
    <img class="wave-unggul" src="images/background/wave.svg" draggable="false" alt=""> 
    <div class="container">
        <div class="justify">
            <h2 class="marker-title">Keunggulan REQ<span>APP+</span></h2>
            <div class="line-divider mb-5"></div>
        </div>
        <div class="row card-justify" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">
            <div class="col-lg-4 col-md-6 col-xs-12 card-separate">
                <div class="card" style="padding:30px; box-shadow: 0 0 3px 3px rgba(100, 100, 100, 0.25);">
                    <div class="text-center card-img-top mb-4" style="height:170px">
                        <img src="images/icon/terintegrasi-logo.svg" draggable="false" alt="">             
                    </div>
                    <div class="card-title" style="height:150px"> 
                        <h5 class="text-center font-weight-bold">Terintegrasi</h5>
                        <p class="text-justify plus-title">Aplikasi REQAPP memiliki 2 tampilan utama yang saling terintegrasi, yaitu tampilan untuk pengguna yang melakukan permintaan dan tampilan untuk pengguna admin. </p>                
                    </div>
                </div>
            </div>
            
            <div class="col-lg-4 col-md-6 col-xs-12 card-separate">
                <div class="card" style="padding:30px; box-shadow: 0 0 3px 3px rgba(100, 100, 100, 0.25);">
                    <div class="text-center card-img-top mb-4" style="height:130px; margin-top:40px">
                        <img src="images/icon/user-friendly-logo.svg" style="margin-right:50px" draggable="false" alt="">             
                    </div>
                    <div class="card-title" style="height:150px"> 
                        <h5 class="text-center font-weight-bold">User Friendly</h5>
                        <p class="text-justify plus-title">
                            Tampilan REQAPP dibuat senyaman mungkin bagi pengguna. Sehingga pengguna dapat melakukan proses permintaan dengan mudah di dalam aplikasi.
                        </p>                
                    </div>
                </div>
            </div>
        
            <div class="col-lg-4 col-md-6 col-xs-12">
                <div class="card" style="padding:30px; box-shadow: 0 0 3px 3px rgba(100, 100, 100, 0.25);">
                    <div class="text-center card-img-top mb-4" style="height:130px; margin-top:40px">
                        <img src="images/icon/realtime-logo.svg" style="margin-right:40px" draggable="false" alt="">             
                    </div>
                    <div class="card-title" style="height:150px"> 
                        <h5 class="text-center font-weight-bold">Realtime</h5>
                        <p class="text-justify plus-title">
                            Sistem REQAPP menganut realtime data, yang artinya setiap permintaan dapat langsung masuk ke sistem admin. 
                        </p>                
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img class="wave-footer" src="images/background/wave-footer.svg" draggable="false" alt=""> 
</section>
<a id="back-to-top" href="#" class="btn btn-success btn-lg back-to-top" role="button"><img src="images/icon/arrow-up.svg" alt=""></a>
<?= $this->include('layout/footer'); ?>
<?= $this->endSection(); ?>
