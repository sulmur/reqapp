<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<?= $this->include('layout/banner'); ?>

<section id="fitur">
    <div class="container">
        <div class="text-center mt-5">
            <h2 class="marker-title mb-4">Fitur REQ<span>APP+</span></h2>
            <p class="marker">Fitur yang ada pada REQAPP+ sangat membantu dalam mengelola logistik<br>pada instansi anda, berikut ini fitur kami :</p>
        </div>
        <table class="table">
            <tr class="d-flex borderless">
                <td class="col-4 text-center p-5" >
                    <img src="images/icon/fitur/modern-interface.svg" class="mb-3" alt="">
                    <h5 class="marker-title mb-3">Modern Interface</h5>
                    <p class="fitur-desc">Memiliki Interface yang modern mengikuti perkembangan zaman.</p>
                </td>
                <td class="col-4 text-center p-5 border-center">
                    <img src="images/icon/fitur/device-support.svg" class="mb-4" alt="">
                    <h5 class="marker-title mb-3">All Device Support</h5>
                    <p class="fitur-desc">Semua perangkat yang anda miliki dapat mengaksesnya.</p>
                </td>
                <td class="col-4 text-center p-5">
                    <img src="images/icon/fitur/backup-system.svg" class="mb-4" alt="">
                    <h5 class="marker-title mb-3">Backup System</h5>
                    <p class="fitur-desc">Memiliki backup system, sehingga data anda tidak akan hilang</p>
                </td>
            </tr>
            <tr class="d-flex">
                <td class="col-4 text-center p-5">
                    <img src="images/icon/fitur/security-system.svg" class="mb-4" alt="">
                    <h5 class="marker-title mb-3">Security System</h5>
                    <p class="fitur-desc">Memiliki keamanan yang  terpercaya, sehingga data anda akan terjaga kerahasiaannya.</p>
                </td>
                <td class="col-4 text-center p-5 border-center">
                    <img src="images/icon/fitur/smart-dashboard.svg" class="mb-4" alt="">
                    <h5 class="marker-title mb-3">Smart Dashboard</h5>
                    <p class="fitur-desc">Memiliki dashboard yang canggih dan pintar, sehingga memudahkan anda dalam menggunakannya.</p>
                </td>
                <td class="col-4 text-center p-5">
                    <img src="images/icon/fitur/report.svg" class="mb-4" alt="">
                    <h5 class="marker-title mb-3">Report</h5>
                    <p class="fitur-desc">Memiliki laporan yang dapat anda akses kapan saja, dan dimana saja.</p>
                </td>
            </tr>
            <tr class="d-flex">
                <td class="col-4 text-center p-5">
                    <img src="images/icon/fitur/technical-support.svg" class="mb-4" alt="">
                    <h5 class="marker-title mb-3">24/7 Technical Support</h5>
                    <p class="fitur-desc">Techinical Support tersedia 24/7 sehingga anda tidak perlu khawatir, karena kami selalu tersedia.</p>
                </td>
                <td class="col-4 text-center p-5 border-center">
                    <img src="images/icon/fitur/full-custom.svg" class="mb-4" alt="">
                    <h5 class="marker-title mb-3">Full Custom</h5>
                    <p class="fitur-desc">Anda dapat melakukan custom sesuai keinginan yang anda mau.</p>
                </td>
                <td class="col-4 text-center p-5"></td>
            </tr>
        </table>
    </div>    
    <img class="fitur-footer" src="images/background/wave-footer.svg" draggable="false" alt=""> 
</section>

<?= $this->include('layout/footer'); ?>
<?= $this->endSection(); ?>